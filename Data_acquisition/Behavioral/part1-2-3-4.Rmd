---
title: "simPEL"
output: word_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r warning=FALSE, message=FALSE}
library(data.table)
library(datasets)
library(readr)
library(knitr)
library(dplyr)
library(ggplot2)
library(rstatix)
library(ggpubr)
library(Rmisc)
```

# PART1
## preparation
```{r warning=FALSE, message=FALSE}
inPath <-"C:/Users/Hopper/PowerFolders/simPEL-EEG/data/beh/part1"

filenames <- list.files(inPath, pattern="*.csv", full.names=TRUE) # List the files
```
``` {r}
# variables that you need
VoI<-c("participant", "date", "learn_resp.keys", "blockID", "trialIdx","biggerResp", "pairID","positionInPair","category","learn_resp.rt")

# create an empty vector 
part1 <- vector() 
```
``` {r}
# then, loop through participants
for (i in filenames){
  
  # retrieve the file
  subfile<-read.csv(i)
  
  # select the variable of interest. In this way, the order will be always the same
  subfile<-subfile[, VoI]
  
  # append it to the dataset
  part1<-rbind(part1, subfile)
}
```
``` {r}
# Now, we need to make the data cleaner for the analysis. Let's get rid of empty rows.
# obs = participants * 7 blocks * 144 trials 
part1 <- part1[!part1$blockID == "",]
```
``` {r} 
# change names
names(part1)[3] <- "resp.keys"
names(part1)[10] <- "rt_org"
```
``` {r} 
# change participant names
part1$participant[part1$participant == 2 & part1$date == "2021-10-12_13h28.07.035"] <- 3
part1$participant[part1$participant == 2 & part1$date == "2021-10-25_08h21.15.346"] <- 4
part1$participant[part1$participant == 2 & part1$date == "2021-10-27_13h12.11.320"] <- 5


```


# PART2
## preparation
```{r warning=FALSE, message=FALSE}
inPath <-"C:/Users/Hopper/PowerFolders/simPEL-EEG/data/beh/part2"

filenames <- list.files(inPath, pattern="*.csv", full.names=TRUE) # List the files
```
``` {r}
# variables that you need
VoI<-c("participant", "learn_resp.keys", "blockID", "trialIdx","biggerResp", "pairID","positionInPair","category","learn_resp.rt", "date")

# create an empty vector 
part2 <- vector() 
```
``` {r}
# then, loop through participants
for (i in filenames){
  
  # retrieve the file
  subfile<-read.csv(i)
  
  # select the variable of interest. In this way, the order will be always the same
  subfile<-subfile[, VoI]
  
  # append it to the dataset
  part2<-rbind(part2, subfile)
}
```
``` {r}
# Now, we need to make the data cleaner for the analysis. Let's get rid of empty rows.
part2 <- part2[!part2$blockID == "",]
```
``` {r} 
# change names
names(part2)[2] <- "resp.keys"
names(part2)[9] <- "rt_org"
```
``` {r} 
# change participant names
part2$participant[part2$participant == 2 & part2$date == "2021-10-13_13h02.22.314"] <- 3
part2$participant[part2$participant == 2 & part2$date == "2021-10-26_08h13.10.658"] <- 4
part2$participant[part2$participant == 2 & part2$date == "2021-10-28_15h07.27.348"] <- 5


```

# PART3
## preparation
```{r warning=FALSE, message=FALSE}
inPath <-"C:/Users/Hopper/PowerFolders/simPEL-EEG/data/beh/part3"

filenames <- list.files(inPath, pattern="*.csv", full.names=TRUE) # List the files
```
``` {r}
# variables that you need
# !!! ACHTUNG! Violation trial
VoI<-c("participant", "remvnv_resp.keys", "blockID", "trialIdx","biggerResp", "pairID","positionInPair","category","remvnv_resp.rt","date") #,"violation")

# create an empty vector 
part3 <- vector() 
```
``` {r}
# then, loop through participants
for (i in filenames){
  
  # retrieve the file
  subfile<-read.csv(i)
  
  # select the variable of interest. In this way, the order will be always the same
  subfile<-subfile[, VoI]
  
  # append it to the dataset
  part3<-rbind(part3, subfile)
}
```
``` {r}
# Now, we need to make the data cleaner for the analysis. Let's get rid of empty rows.
# participant * 476 trials
part3 <- part3[!part3$blockID == "",]
```
``` {r} 
# change names
names(part3)[2] <- "resp.keys"
names(part3)[9] <- "rt_org"
```
``` {r} 
# ACHTUNG! Change block A1 as "reminder"
part3$blockID[part3$blockID == "A1"] <- "reminder"
```
``` {r} 
# change participant names
part3$participant[part3$participant == 2 & part3$date == "2021_Oct_14_1155"] <- 3

```


# Combine all of them in one and get 
``` {r}
df <- rbind(part1, part2, part3)
```

## RT preparations
```{r}
df$rt <- df$rt_org
df <- df[!df$trialIdx == 1,]

# Calculate Standard Deviations
test <- aggregate(rt_org ~ participant, df, sd, na.rm = T)

# Merge these two for having rt and participant level sd
df_sleep <- merge(x = df, y = test, by = c("participant"), all.x = TRUE)
```
```{r}
# Higher RTs: Trials are 3 SD slower than the mean bye bye! WHAT ABOUT 4 SD?
df_sleep$rt[df_sleep$rt_org.x >= (3*df_sleep$rt_org.y)] <- NA
```
```{r}
df_sleep$rt[df_sleep$rt <= 0.05] <- NA # Excluding RTs below 0.05 seconds (50 ms)
```

#PLOT RT
```{r}
# prep
df_agg <- aggregate(rt_org ~ blockID + positionInPair * participant, data = df, mean)
  
df_agg$blockID <- factor(df_agg$blockID, levels=c("A1", "A2","A3","A4","A5","A6","A7", "A8","A9","A10","A11","A12","A13", "A14", "reminder"))
df_agg <- df_agg[!df_agg$positionInPair == "singleton", ]
df_agg$positionInPair <- factor(df_agg$positionInPair, levels=c("1", "2"))

# plot individual
df_agg %>% 
    filter(!(positionInPair %in% c(NA))) %>% 
    ggplot(aes(x=blockID, y=rt_org, group = positionInPair, color = positionInPair)) + geom_line() +  geom_point() + theme_classic() + labs(x = "Block", y = "RTs") + facet_wrap(vars(participant)) + scale_color_manual(values=c("lightblue", "darkblue"))

# all
df_agg %>% 
    filter(!(positionInPair %in% c(NA))) %>% 
    ggplot(aes(x=blockID, y=rt_org, group = positionInPair, color = positionInPair)) + geom_line() +  geom_point() + theme_classic() + labs(x = "Block", y = "RTs")

```

# PLOT RT DIFFERENCES
```{r}
# BURASI CALISMIYOR
df1 <- df_agg[df_agg$positionInPair == 1,]
df1 <- na.omit(df1)
df2 <- df_agg[df_agg$positionInPair == 2,]
df2 <- na.omit(df2) 

df_diff <- merge(df1, df2, by = c("participant","blockID"))
df_diff$rt_diff <- df_diff$rt_org.x - df_diff$rt_org.y

# plot
df_diff %>% 
    ggplot(aes(x=blockID, y= rt_diff)) + geom_line() +  geom_point() + theme_classic() + labs(x = "Block", y = "RTs") + facet_wrap(vars(participant))

```


# PART3: RTs violation vs no-violation (v_single - nv_single)
```{r}
#prep
dfV <- part3[which(part3$violation == "v_single"), ]
dfNV <- part3[which(part3$violation == "nv_single"), ]
df_vio <- rbind(dfV, dfNV)
df_vio$violation <- factor(df_vio$violation, levels=c("v_single", "nv_single"))

df_vio %>%
ggplot(aes(x=violation, y=rt_org, fill=violation)) + geom_boxplot() + theme_classic2() + scale_x_discrete(name = "Violation", labels=c("v_single" = "violation", "nv_single" = "no-violation")) + scale_fill_manual(values=c("chocolate1", "blueviolet")) + ylab("RTs") + facet_wrap(vars(participant))

# all
ggplot(df_vio, aes(x = violation, y = rt_org)) +
  geom_bar(aes(violation, rt_org, fill = violation),
           position="dodge",stat="summary") +
  #geom_point() +
  #geom_line(aes(group = participant), size = 0.75) +
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 ) +
  theme_classic2() +
  scale_x_discrete(name = "Violation", labels=c("v_single" = "violation", "nv_single" = "no-violation")) +
  ylab("RTs") +
  scale_fill_manual(values=c("chocolate1", "blueviolet")) +
  ylim(0,1)

```

# PART4: Old responses
## preparation
```{r warning=FALSE, message=FALSE}
inPath <-"C:/Users/Hopper/PowerFolders/simPEL-EEG/data/beh/part4"

filenames <- list.files(inPath, pattern="*.csv", full.names=TRUE) # List the files
```
``` {r}
# variables that you need
VoI<-c("participant", "recog_resp.rt","recog_resp.corr", "blockID", "trialIdx","pairID","violation","oldOrNew","date")

# create an empty vector 
part4 <- vector() 
```
``` {r}
# then, loop through participants
for (i in filenames){
  
  # retrieve the file
  subfile<-read.csv(i)
  
  # select the variable of interest. In this way, the order will be always the same
  subfile<-subfile[, VoI]
  
  # append it to the dataset
  part4<-rbind(part4, subfile)
}
```
``` {r}
# Now, we need to make the data cleaner for the analysis. Let's get rid of empty rows.
# participant * 120 trials
part4 <- part4[!part4$blockID == "",]
```
``` {r} 
# change names
names(part4)[2] <- "rt"
names(part4)[3] <- "corrAns"
```
``` {r} 
# change participant names
part4$participant[part4$participant == 2 & part4$date == "2021_Oct_14_1253"] <- 3
part4$participant[part4$participant == 2 & part4$date == "2021_Oct_27_1050"] <- 4
part4$participant[part4$participant == 2 & part4$date == "2021_Oct_29_1705"] <- 5



```


```{r}
df_old <- part4[part4$oldOrNew == "old", ] # only old ones
df_old$violation <- factor(df_old$violation, levels=c("v_single", "nv_single"))

## Plot

df_agg2 <- aggregate(corrAns ~ violation + participant, data = df_old, mean)


# plot
ggplot(df_old, aes(violation, corrAns))+
  geom_bar(aes(violation, corrAns, fill = violation),
           position="dodge",stat="summary")+
  #geom_point()+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  theme_classic2()+ 
  scale_x_discrete(name = "Violation", labels=c("v_single" = "violation", "nv_single" = "no-violation")) + ylab("Prop Old Responses") + scale_fill_manual(values=c("chocolate1", "blueviolet")) + facet_wrap(vars(participant))

# all 
ggplot(df_agg2, aes(x = violation, y = corrAns)) +
  geom_bar(aes(violation, corrAns, fill = violation),
           position="dodge",stat="summary", width = 0.5) +
  geom_point() +
  geom_line(aes(group = participant), size = 0.75) +
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 ) +
  theme_classic2() +
  scale_x_discrete(name = "Violation", labels=c("v_single" = "violation", "nv_single" = "no-violation")) +
  ylab("Prop Old Responses") +
  scale_fill_manual(values=c("chocolate1", "blueviolet"))

  
```

# PART 6
## preparation
```{r warning=FALSE, message=FALSE}
inPath <-"C:/Users/Hopper/PowerFolders/simPEL-EEG/data/beh/part6"

filenames <- list.files(inPath, pattern="*.csv", full.names=TRUE) # List the files
```
``` {r}
# variables that you need
# !!! ACHTUNG! Violation trial
VoI<-c("participant", "assoc_resp.corr", "blockID", "trialIdx", "pairID","violation","assoc_resp.rt","date")

# create an empty vector 
part6 <- vector() 
```
``` {r}
# then, loop through participants
for (i in filenames){
  
  # retrieve the file
  subfile<-read.csv(i)
  
  # select the variable of interest. In this way, the order will be always the same
  subfile<-subfile[, VoI]
  
  # append it to the dataset
  part6<-rbind(part6, subfile)
}
```
``` {r}
# Now, we need to make the data cleaner for the analysis. Let's get rid of empty rows.
# participant * 80 trials
part6 <- part6[!part6$blockID == "",]
```
``` {r} 
# change names
names(part6)[2] <- "corrAns"
names(part6)[7] <- "rt"
```
``` {r} 
# get rid of practice truals
part6 <- part6[!part6$blockID == "A0", ]

```
``` {r} 
# change participant names
part6$participant[part6$participant == 2 & part6$date == "2021_Oct_14_1316"] <- 3
part6$participant[part6$participant == 2 & part6$date == "2021_Oct_27_1113"] <- 4
part6$participant[part6$participant == 2 & part6$date == "2021_Oct_29_1734"] <- 5

```

## associoation memory performance
``` {r} 
aggregate(corrAns ~ participant, data = part6, mean)

## have a look the recognition only for correctly remembered ones
part4$pairID <- as.numeric(part4$pairID)

df_as <- merge(part4, part6, by = c("participant", "pairID"))

df_as <- df_as[!df_as$corrAns.y == 0, ]


# prep
df_as <- df_as[df_as$oldOrNew == "old", ] # only old ones
df_as$violation.x <- factor(df_as$violation.x, levels=c("v_single", "nv_single"))

# plot
ggplot(df_as, aes(violation.x, corrAns.x))+
  geom_bar(aes(violation.x, corrAns.x, fill = violation.x),
           position="dodge",stat="summary")+
  #geom_point()+
  stat_summary(fun.data = "mean_cl_boot", size = 0.8, geom="errorbar", width=0.2 )+
  theme_classic2()+ 
  scale_x_discrete(name = "Violation", labels=c("v_single" = "violation", "nv_single" = "no-violation")) + ylab("Prop Old Responses") + scale_fill_manual(values=c("chocolate1", "blueviolet")) + facet_wrap(vars(participant))



```



