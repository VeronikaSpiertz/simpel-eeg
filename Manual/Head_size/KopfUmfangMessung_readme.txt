
German:

Damit wir direkt die passende Kappe für Dich vorbereiten können, bräuchten wir deinen Kopfumfang. Somit könnten wir wertvolle Zeit sparen.
Bitte miss dafür Deinen Kopf an der Stelle mit dem größten Umfang (siehe Foto im Anhang). Du kannst uns den Umfang (in cm) einfach per Mail mitteilen.
Wenn du kein Maßband hast, könntest du eine Schnur oder einen langen Papierstreifen nehmen, damit deinen Kopfumfang markieren und die Länge mit einem Lineal nachmessen.

Vielen Dank für deinen Unterstützung.

English:
We would like to know your head circumference. So that we can directly prepare the right cap for you. This would save us valuable time.
Please measure your head at the point with the largest circumference (see photo attached). You can tell us the circumference (in cm) simply by mail.
If you don't have a tape measure, you could take a string or a long paper strip, mark your head circumference with it and measure the length with a ruler.

Thank you for your support.

